package rov.com.bcomunnity.services.TO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TOResult {

    @Expose
    private String message;

    @Expose
    @SerializedName("place_id")
    private String placeId;

    public String getMessage() {
        return message;
    }

    public String getPlaceId() {
        return placeId;
    }
}
