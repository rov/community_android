package rov.com.bcomunnity.models;

import com.google.gson.annotations.Expose;

public class Place {

    @Expose
    private String name;

    @Expose
    private String address;

    @Expose
    private String latitude;

    @Expose
    private String longitude;

    @Expose
    private int beverage;

    public Place(String name, String address, String latitude, String longitude, int beverage) {
        this.name = name;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.beverage = beverage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getBeverage() {
        return beverage;
    }

    public void setBeverage(int beverage) {
        this.beverage = beverage;
    }
}
