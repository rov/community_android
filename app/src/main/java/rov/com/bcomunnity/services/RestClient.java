package rov.com.bcomunnity.services;

import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.RequiresHeader;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import rov.com.bcomunnity.services.TO.TOResult;
import rov.com.bcomunnity.models.Place;

@Rest(rootUrl = "https://c7q5vyiew7.execute-api.us-east-1.amazonaws.com", converters = { GsonHttpMessageConverter.class })
public interface RestClient {

    @Post("/prod/places")
    @RequiresHeader("x-api-key")
    TOResult postPlace(final Place place);

    void setHeader(String name, String value);
}