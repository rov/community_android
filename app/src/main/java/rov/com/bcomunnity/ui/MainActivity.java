package rov.com.bcomunnity.ui;

import android.app.ProgressDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import rov.com.bcomunnity.R;
import rov.com.bcomunnity.services.TO.TOResult;
import rov.com.bcomunnity.models.Place;
import rov.com.bcomunnity.services.RestClient;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    ProgressDialog progress;

    @RestService
    protected RestClient restClient;

    @ViewById(R.id.edit_text_name)
    EditText editTextName;

    @ViewById(R.id.edit_text_address)
    EditText editTextAddress;

    @ViewById(R.id.edit_text_latitude)
    EditText editTextLatitude;

    @ViewById(R.id.edit_text_longitude)
    EditText editTextLongitude;

    @ViewById(R.id.spinner)
    Spinner spinner;

    @ViewById(R.id.fab)
    FloatingActionButton fab;

    @AfterViews
    protected void afterViews() {
        restClient.setHeader("x-api-key", "IfXJnQVdjo1fI4z6OQTWB6RPJ8Qs4JbcaDOZ83vt");
    }

    @Click(R.id.fab)
    public void onClick() {
        Place place = prepareData();
        postPlace(place);
    }

    private Place prepareData() {

        final String name = editTextName.getText().toString();
        final String address = editTextAddress.getText().toString();
        final String latitude = editTextLatitude.getText().toString();//"-23.5976730";
        final String longitude = editTextLongitude.getText().toString();//"-46.6820624";
        final int beverage = spinner.getSelectedItemPosition() + 1;

        Place place = new Place(name, address, latitude, longitude, beverage);

        return place;
    }

    @Background
    protected void postPlace(final Place place) {
        try {
            showProgressDialog();
            TOResult toResult = restClient.postPlace(place);
            showSnackBarMsg(fab, toResult.getMessage());
            dismissProgressDialog();
        } catch (Exception e) {
            dismissProgressDialog();
            e.getMessage();
            showSnackBarMsg(fab, "error");
        }
    }

    @UiThread
    public void showProgressDialog() {
        progress = ProgressDialog.show(this, "loading",
                "wait", true);
    }

    @UiThread
    public void dismissProgressDialog() {
        if(progress != null) {
            progress.dismiss();
        }
    }

    protected void showSnackBarMsg(final View view, final String msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }
}
